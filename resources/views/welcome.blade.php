@extends('layouts.app')

@section('content')
{{--        @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif--}}
{{--    <div class="A10">
        <img src="../public/assets/sources-homepage/A110/Alpine-A110-1.jpg" width="100%">
    </div>--}}

{{--navbar--}}
<div class="navbar-fixed">
    <nav class="navbar rgba-blue-strong fixed-top scrolling-navbar">
        <a href="{{ url('/') }}" class="brand-logo"><img src="{{url('assets/sources-homepage/logo/logo-white.png')}}" width="180"></a>
        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            <li><a href="{{ route('configurateur') }}">Configurateur</a></li>
            <li><a href="collapsible.html">Connexion</a></li>
            <li><a href="collapsible.html">Inscription</a></li>
            <li><a class='dropdown-trigger btn btn-nav' href='#' data-target='dropdown1'>Menu Home</a></li>
        </ul>
        <!-- Dropdown Structure -->
        <ul id='dropdown1' class='dropdown-content'>
            <li><a href=".section1">Agilité</a></li>
            <li><a href=".section2">Design</a></li>
            <li><a href=".section3">Conception</a></li>
            <li><a href=".section4">Motorisation</a></li>
            <li><a href=".section5">Technologie</a></li>
            <li><a href=".section6">Intérieur</a></li>
            <li><a href=".section7">Caractéristique</a></li>
            <li><a href=".section8">Version</a></li>
            <li><a href=".section9">Galerie photos</a></li>
        </ul>
    </nav>
</div>
<ul class="sidenav" id="mobile-demo">
    <li><a href="{{ route('configurateur') }}">CONFIGURATEUR</a></li>
    <li><a href=".section1">Agilité</a></li>
    <li><a href=".section2">Design</a></li>
    <li><a href=".section3">Conception</a></li>
    <li><a href=".section4">Motorisation</a></li>
    <li><a href=".section5">Technologie</a></li>
    <li><a href=".section6">Intérieur</a></li>
    <li><a href=".section7">Caractéristique</a></li>
    <li><a href=".section8">Version</a></li>
    <li><a href=".section9">Galerie photos</a></li>
</ul>
{{--End NavBar--}}
<div id="startscreen">
    <div id="title"><img src="{{url('/assets/sources-homepage/logo/logo-white.png')}}" width="26%"></div>
    <div id="test"><img class="PreHome" src="{{url('/assets/sources-homepage/A110/Alpine-A110-1.jpg')}}"></div>
    <div id="triangle-down"></div>
    <div id="triangle-up"></div>
    <a href="#" id="button" class="boton">Entrer</a>
</div>

<div id="menuContent">
    <ul>
        <li><a href=".section1">Agilité</a></li>
        <li><a href=".section2">Design</a></li>
        <li><a href=".section3">Conception</a></li>
        <li><a href=".section4">Motorisation</a></li>
        <li><a href=".section5">Technologie</a></li>
        <li><a href=".section6">Intérieur</a></li>
        <li><a href=".section7">Caractéristique</a></li>
        <li><a href=".section8">Version</a></li>
        <li><a href=".section9">Galerie photos</a></li>
    </ul>
    <div id="menuClose">✕</div>
</div>

<main>
    {{--Header--}}
    <section class="section">
        <div class="hero">
            <div class="content-wrapper"></div>
                <h1 class="home-title">ALpine A110 Légende</h1>
{{--                <div class='console-container'><span id='text'></span>
                <div class='console-underscore' id='console'>&#95;</div>--}}
            </div>
        </div>
    </section>
     {{--Section Agilité--}}
    <section class="section1">
        <div class="agilite-section">
            <div class="row responsive-img img-entrer">
                <div class="col l4 m12 s12 text-entrer">
                    <h4 class="title-entrer"> Agilité absolue </h4>
                    <p class="text-entrer-p"> Poids plume, design élégant, l’A110 est la nouvelle voiture de sport signée Alpine.
                        Séduisante, légère et joueuse, l'Alpine A110 se veut fidèle à l’esprit de la célèbre berlinette.
                        Equipé d’un moteur central arrière, ce coupé sport résolument moderne est agile, compact et léger.
                        Aussi à l’aise sur circuit que sur routes de montagne, l’A110 combine élégance et confort au quotidien.</p>
                </div>
            </div>
        </div>
    </section>
    {{--Section Design--}}
    <section class="section2">
        <div class="slider">
            <ul class="slides">
                <li>
                    <img class="responsive-img" src="{{url('/assets/sources-homepage/design/Visuel_1_desktop.jpg')}}">
                    <div class="caption center-align">
                        <h4>Design moderne!</h4>
                        <h6>L'A110 une structure équilibrée et souple.</h6>
                    </div>
                </li>
                <li>
                    <img class="responsive-img" src="{{url('/assets/sources-homepage/design/Visuel_3_desktop.jpg')}}">
                    <div class="caption left-align">
                        <h4>Carrosserie en aluminium</h4>
                        <h6>L’aluminium est partout.</h6>
                    </div>
                </li>
                <li>
                    <img class="responsive-img" src="{{url('/assets/sources-homepage/design/duo-left.jpg')}}">
                    <div class="caption right-align">
                        <h4>Une finition parfaite</h4>
                        <h6>Une peau qui épouse au plus près les organes mécaniques.</h6>
                    </div>
                </li>
                <li>
                    <img class="responsive-img" src="{{url('/assets/sources-homepage/design/duo-right.jpg')}}">
                    <div class="caption center-align">
                        <h4>Une finition parfaite</h4>
                        <h6>Une vaste lunette arrière galbée.</h6>
                    </div>
                </li>
            </ul>
        </div>
        <div class="container img-design">
            <div class="row">
                <div class="col s12 m6 l3">
                    <div class="cadre-img">
                        <img class='responsive-img block-img-design' src="{{url('/assets/sources-homepage/design/duo-left.jpg')}}"/>
                    </div>
                </div>
                <div class="col s12 m6 l3">
                    <div class="cadre-img">
                        <img class='responsive-img block-img-design' src="{{url('/assets/sources-homepage/design/duo-right.jpg')}}"/>
                    </div>
                </div>
                <div class="col s12 m6 l3 test5">
                    <div class="cadre-img">
                        <img class='responsive-img block-img-design' src="{{url('/assets/sources-homepage/design/Visuel_1_desktop.jpg')}}"/>
                    </div>
                </div>
                <div class="col s12 m6 l3 test5">
                    <div class="cadre-img">
                        <img class='responsive-img block-img-design' src="{{url('/assets/sources-homepage/design/Visuel_3_desktop.jpg')}}" width="651" height="609"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col s12 l6 m6">
                    <h5 class="text-design-title"> Silhouette </h5>
                    <p class="text-design">L’Alpine A110, c’est avant tout un dessin tendu réalisé d’un seul trait.
                        C’est aussi une peau qui épouse au plus près les organes mécaniques,
                        une assise et un centre de gravité abaissés, une vaste lunette arrière galbée et des
                        flancs creusés qui soulignent des passages de roues saillants. </p>
                </div>
                <div class="col s12 l6 m6">
                    <h5 class="text-design-title"> Carrosserie en aluminium </h5>
                    <p class="text-design"> L’aluminium est partout. Utilisé pour la carrosserie et le soubassement rivetés et soudés,
                        ce métal noble ultraléger assure à l'A110 une structure équilibrée et souple. Clins d'œil aux anciennes Alpine :
                        les flancs creusés et la nervure centrale sur le capot. </p>
                </div>
            </div>
        </div>
    </section>
    {{--Section conception--}}
    <section class="section3">
        <div class="container">
            <div class="row">
            <h3 class="conception-title col l12 s12 m12">Conception</h3>
            </div>
            <div class="row img-style-slide">
                <div class=" col s12 m12 l12 baSlider">
                    <figure class="img-voiture-conception">
                        <div id="divider"></div>
                    </figure>
                    <input type="range" min="0" max="100" value="50" id="slider" oninput="moveDivider()">
                </div>
            </div>
            <div class="row">
                <div class="col l3">
                    <h6><b>Légèreté</b></h6>
                    <p class="text-conception">Pour une voiture sportive, le poids est un élément essentiel.
                    La répartition des masses optimale de l’A110, ses dimensions compactes et
                        la légèreté de sa carrosserie en aluminium lui confèrent plaisir de conduite et agilité.</p>
                </div>
                <div class="col l6">
                    <img class="img-conception" src="{{url('/assets/sources-homepage/conception/visuel_legerete_2_desktop.jpg')}}">
                </div>
                <div class="col l3">
                    <h6 class="conception-title2"><b>Robustesse</b></h6>
                    <p class="text-conception2">L’A110 tire de sa structure en aluminium robustesse, extrême agilité et maniabilité.
                        Le design épuré et les éléments de carrosserie rivetés et soudés font de nouveau gagner en légèreté et en robustesse.</p>
                </div>
            </div>
        </div>
    </section>
    {{--Section Motorisation--}}
    <section class="section4">
        <div class="container">
            <div class="row">
                <div class="col l6 m12 s12">
                    <p class="motorisation-texte">
                        Placé en position centrale arrière, le moteur turbocompressé 4 cylindres fait battre le cœur de l’A110.
                        Réglé par les ingénieurs Alpine pour offrir une meilleure réponse à l'accélération, le moteur à injection directe 1,8 L.
                        joue la carte de la performance. La signature sonore sportive ajoute quant à elle une part d’émotion.
                    </p>
                    <div class="grid">
                        <figure class="effect-layla">
                            <img class="responsive-img img-motorisation" src="{{url('/assets/sources-homepage/motorisation/turbo_desktop.png')}}" alt="img04"/>
                            <figcaption>
                                <h4>Turbo</h4>
                                <p>moteur turbocompressé</p>
                                <a href="#">View more</a>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <div class="col l6 s12 m12">
                    <p class="motorisation-title">Motorisation</p>
                    <video class="video" controls autoplay loop>
                        <source src="{{url('assets/sources-homepage/motorisation/MOTEUR_CINEMAGRAPH-.mov')}}" type="video/mp4">
                        Votre navigateur ne supporte pas la balise vidéo.
                    </video>
                    <p class="performance-title">Performances</p>
                </div>
            </div>
            <div>
                La légèreté de la structure associée à un moteur turbocompressé font de l’A110 un modèle ultra performant doté
                d’un rapport poids/puissance de 4,29 kg/ch. La preuve en est, l’A110 peut passer de 0 à 100 km/h en seulement 4,5 secondes.
            </div>
            <div>
                Transmission Assuré par une boîte à double embrayage, le système de transmission garantit des passages de vitesse fluides et rapides.
                Les palettes en aluminium permettent au conducteur un contrôle parfait du véhicule.
            </div>
        </div>

    </section>
    {{--Section techonologie--}}
    <section class="section5">
        <h3 class="technologie-title">Technologie</h3>
        <div class="container">
            <div class="row">
                <div class="col l6 m12 s12">
                    <figure class="snip1482">
                        <figcaption>
                            <h3>Suspensions</h3>
                            <p>L’A110 tient son agilité et sa précision de pilotage de son système de suspension à double triangulation.</p>
                        </figcaption>
                        <a href="#"></a><img src="{{url('/assets/sources-homepage/technologie/Technical-front-wheel-A110_mobile.jpg')}}" alt="sample45" />
                    </figure>
                </div>
                <div class="col l6 m12 s12">
                    <figure class="snip1482">
                        <figcaption>
                            <h3>Freins</h3>
                            <p>Les freins de l'équipementier Brembo offrent ce qui se fait de mieux en termes d’endurance et de distance de freinage.</p>
                        </figcaption>
                        <a href="#"></a><img src="{{url('assets/sources-homepage/technologie/Technical-rear-wheel-A110_mobile.jpg')}}" alt="sample59" />
                    </figure>
                </div>
            </div>
            <div class="row">
                <p>
                    La nouvelle A110 couple les singularités propres à Alpine aux dernières technologies.
                    La boîte de vitesse automatique et le choix entre trois modes de conduite permettent une meilleure appropriation du véhicule.
                </p>
                <video class="video offset-l1" controls autoplay loop>
                    <source src="{{url('assets/sources-homepage/technologie/Center-of-gravity-FR_LOW.mov')}}" type="video/mp4">
                    Votre navigateur ne supporte pas la balise vidéo.
                </video>
            </div>
        </div>
    </section>
    <section class="section6">
        <div class="parallax-container">
            <div class="parallax"><img src="{{url('assets/sources-homepage/interieur/interieur_2_desktop.png')}}"></div>
        </div>
    </section>
    <section>
        <div class="agilite-section">
            <div class="row responsive-img img-interieur">
                <div class="col l5 offset-l7 m12 s12 text-interieur">
                    <h4 class="title-entrer"> Intérieur </h4>
                    <p class="text-entrer-p">A l’intérieur, l’émotion vient du contraste entre les matières chaudes et les matières froides.
                        Les éléments en aluminium apparents se marient avec du cuir. Le carbone complète l’ambiance sportive.
                        La présence d’une console centrale surélevée et de sièges baquets légers vont de paire avec l’esprit de légèreté de la structure.
                        La climatisation et la connexion au smartphone garantissent eux liberté et confort.
                        Les sièges
                        Avec seulement 13,1 kg chacun, les sièges baquet offrent un confort maximal en piste et participent de l’extrême légèreté de l’A110.
                        Des sièges six-voies réglables, toujours très légers, sont également proposés en option.</p>
                </div>
            </div>
        </div>
    </section>
    {{--Section caractéristique--}}
    <section class="section7">
        <div class="container">
            <div class="row">
                <h3 class="caracteristique-title">Caractéristiques</h3>
                <div class="col l6 m6 s12">
                    <img class="responsive-img zoom conception-img" src="{{url('/assets/sources-homepage/caractéristiques/performance_desktop.png')}}">
                </div>
                <div class="col l6 m6 s12">
                    <img class="responsive-img zoom conception-img" src="{{url('/assets/sources-homepage/caractéristiques/transmission_desktop.png')}}">
                </div>
            </div>
            <div class="row">
                <div class="col l8 offset-l2 responsive-table">
                    <table class="centered">
                        <tbody>
                        <tr>
                            <td><b>Puissance moteur</b></td>
                            <td>252 ch (185kw)</td>
                        </tr>
                        <tr>
                            <td><b>Accélération de 0 à 100 km/h</b></td>
                            <td>4,5 secondes</td>
                        </tr>
                        <tr>
                            <td><b>Vitesse maximale (limitée éléctroniquement)</b></td>
                            <td>250 km/h</td>
                        </tr>
                        <tr>
                            <td><b>Consommation en cycle mixte*</b></td>
                            <td>6,4 l/100</td>
                        </tr>
                        <tr>
                            <td><b>Émissions CO2 en cycle mixte*</b></td>
                            <td>144 g/km</td>
                        </tr>
                        <tr>
                            <td><b>Boîte de vitesse</b></td>
                            <td>Automatique double embrayage à 7 rapports</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    {{--Section Versions--}}
    <section class="section8">
        <div class="row block-img-title-versions">
            <img class="col l4 m4 s12 img-versions" src="{{url('/assets/sources-homepage/versions/COMPO-PURE.png')}}">
                <h3 class="col l4 m4 s12 versions-title">Versions</h3>
            <img class="col l4 m4 s12 img-versions" src="{{url('/assets/sources-homepage/versions/COMPO-LEGENDE.png')}}">
        </div>
        <div class="container">
            <div class="row marg-versions">
                <div class="col l6 m6 s12">
                    <h5 class="versions-title-block">A110 Pure</h5>
                    <p class="text-versions">Légère et conçue avant tout pour le plaisir du pilote,
                        l’A110 Pure est la version la plus fidèle à l’esprit de la mythique berlinette qui remporta le Rallye Monte-Carlo en 1973.
                        Équipée de sièges baquet Sabelt, l’A110 Pure combine agilité et précision de pilotage. À l’allure volontairement sportive,
                        cette version est habillée de sièges en cuir-microfibre,
                        les finitions intérieures pariant sur la fibre de carbone mat.</p>
                </div>
                <div class="col l6 m6 s12">
                    <h5 class="versions-title-block2">A110 Légende</h5>
                    <p class="text-versions2">Dotée du même groupe motopropulseur et des mêmes réglages de suspension que l'A110 Pure,
                        la version Légende est fidèle à la philosophie Alpine : l’agilité absolue. Affichant le caractère d’une GT,
                        l’A110 Légende se distingue par un choix d’assises grand confort avec des sièges réglables à six voies et une sellerie en cuir noir ou brun.
                        L’habitacle en fibre de carbone brillant et les jantes 18 pouces Légende subliment la personnalité raffinée de la version Légende.
                        {{--Quant aux capteurs de stationnement avec caméra de recul,
                        ils procurent à l’A110 Légende maniabilité et simplicité d’utilisation au quotidien.--}}</p>
                </div>
            </div>
            <div class="row">
                <div class="col offset-l1 offset-xl2 img-svg-versions">
                    <svg>
                        <defs>
                            <filter id="turbulent-dissolve" x="0%" y="0%">
                                <feTurbulence type="fractalNoise" baseFrequency=".012"/>
                                <feColorMatrix type="luminanceToAlpha"/>
                                <feComponentTransfer>
                                    <feFuncA type="linear" slope="0">
                                        <animate attributeName="slope" values="0;0;0;0;0;0.5;1;1.5;2;2;2;2;2;2;1.5;1;0.5;0" dur="8s" repeatCount="indefinite"/>
                                    </feFuncA>
                                </feComponentTransfer>
                                <feComponentTransfer>
                                    <feFuncA type="discrete" tableValues="0 1"/>
                                </feComponentTransfer>
                                <feGaussianBlur stdDeviation="1"/>
                                <feComposite operator="in" in="SourceGraphic" result="overlay"/>
                                <feImage xlink:href="{{url('/assets/sources-homepage/versions/ALPINE-LEGENDE-1.png')}}" width="800" height="600" result="underlay"/>
                                <feComposite operator="over" in="overlay" in2="underlay"/>
                            </filter>
                        </defs>
                        <image filter="url(#turbulent-dissolve)" width="800" height="600" xlink:href="{{url('/assets/sources-homepage/versions/ALPINE-PURE-1.png')}}"/>
                    </svg>
                </div>
            </div>
        </div>
    </section>
    {{--Section Gallerie--}}
    <section class="section9">
        <h3 class="conception-title">Galerie</h3>
        <div class="container">
            <div class="row img-gallery-responsive">
                <div class="gallery col l12 offset-l1 xl12 offset-xl1 m12 s12">
                    <div class="img-w"><img src="{{url('/assets/sources-homepage/galerie/A110_LEGENDE_5.jpg')}}" alt="" /></div>
                    <div class="img-w"><img src="{{url('/assets/sources-homepage/galerie/A110_LEGENDE_9.jpg')}}" alt="" /></div>
                    <div class="img-w"><img src="{{url('/assets/sources-homepage/galerie/A110_PE_1.jpg')}}" alt="" /></div>
                    <div class="img-w"><img src="{{url('/assets/sources-homepage/galerie/A110_PE_7.jpg')}}" alt="" /></div>
                    <div class="img-w"><img src="{{url('/assets/sources-homepage/galerie/A110_PE_9.jpg')}}" alt="" /></div>
                    <div class="img-w"><img src="{{url('/assets/sources-homepage/galerie/A110_PURE_4.jpg')}}" alt="" /></div>
                    <div class="img-w"><img src="{{url('/assets/sources-homepage/galerie/A110_PURE_6.jpg')}}" alt="" /></div>
                    <div class="img-w"><img src="{{url('/assets/sources-homepage/galerie/A110_PURE_8.jpg')}}" alt="" /></div>
                    <div class="img-w"><img src="{{url('/assets/sources-homepage/galerie/A110_LEGENDE_1.jpg')}}" alt="" /></div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="footer">
            <div class="footer_center">
                <h5 class="text-footer">Copyright ©  MoYvalpine by Morgan V et Yverick DC</h5>
            </div>
        </div>
    </footer>
</main>
@endsection
