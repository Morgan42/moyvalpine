import ReactDOM from 'react-dom';
import React, { Component } from 'react';
const { Carousel, Modal, Button   } = require('react-materialize');

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            modele: 'no modele choose',
            price: 0,
            color: 'no color choose',
            colorPrice: 0,
            rims: 'No rims choose',
            rimsPrice: 0,
            seat: 'no seat choose',
            seatPrice: 0,
            listEquipment: [],
            equipmentPrice: 0,
            listAccessorie:[],
            accessoriePrice: 0,
        }

        this.modele = [
            {
                name: "Pure",
                price: 54700,
            },
            {
                name: "Legend",
                price: 58500,
            }

        ]

        this.color = [
            {
                name : "Teinte spéciale Bleue Alpine",
                price : 1800,
            },
            {
                name : "Teinte métalisée noire",
                price : 0,
            },
            {
                name : "Peinture Blanc opaque",
                price : 0,
            }
        ]

        this.rims = [
            {
                name: "Standard",
                price: 0
            },
            {
                name: "Serac",
                price: 1000
            },
            {
                name: "Legende",
                price: 0
            }
        ]

        this.seat = [
            {
                name: "Sièges baquets en cuir noir Dinamica",   // DISPO QUE POUR MODELE PURE
                price: 0
            },
            {
                name: "Sièges confort en cuir noir perforé",    // DISPO QUE POUR MODELE PURE
                price: 800
            },
            {
                name: "Siège confort en cuir noir",           // DISPO QUE POUR MODELE LEGEND
                price: 0
            },
            {
                name: "Siège confort en cuir brun",             // DISPO QUE POUR MODELE LEGEND
                price: 800
            }
        ]

        this.equipment = [
            {
                name: "Pack héritage",
                price: 180
            },
            {
                name: "Repose-pieds passager en aluminium",
                price: 96
            },
            {
                name: "Alpine telemetrics",
                price: 204
            },
            {
                name: "Système audio Focal",
                price: 600
            },
            {
                name: "Système audio Focal Premium",
                price: 1200
            },
            {
                name: "Système audio Standard",
                price: 0
            },
            {
                name: "Retroviseur intérieur électochrome",
                price: 0
            },
            {
                name: "Retroviseurs exterieur chauffants rabattable",
                price: 504
            },
            {
                name: "Pack de rangement",
                price: 504
            },
            {
                name: "Regulateur/Limiteur de vitesse",
                price: 0
            },
            {
                name: "Aide au stationnement arrière",
                price: 420
            },
            {
                name: "Aide au stationnement avant & arrière",
                price: 750
            },
            {
                name: "Aide au stationnement avant & arrière et caméra de recul",
                price: 1200
            },
            {
                name: "Echappement sport actif",
                price: 1500
            },
            {
                name: "Système de freinage Haute-Perf 320mm",
                price: 1000
            },
            {
                name: "Assistance au freinage d'urgence",
                price: 0
            },
            {
                name: "Airbags frontaux conducteur et passager",
                price: 0
            },
            {
                name: "Etrier de frein bleu Alpine",
                price: 384
            },
            {
                name: "Logo Alpine sur ailes avant",
                price: 120
            },
            {
                name: "Etrier de frein Gris Anthracite",
                price: 0
            },
            {
                name: "Pédalier en aluminium",
                price: 120
            },
            {
                name: "Harmonie carbone mat",
                price: 0
            },
            {
                name: "Logo au centre du volant en Bleu Alpine",
                price: 84
            },
            {
                name: "Sièges chauffants",
                price: 400
            },
        ]

        this.accessorie = [
            {
                name: "Extincteur 1kg avec nanometre",
                price: 22
            },
            {
                name: "Chaine à neige Prenium Grip",
                price: 336
            },
            {
                name: "Alarme",
                price: 543
            },
            {
                name: "Protection prise OBD",
                price: 99
            },
            {
                name: "Kit de sécurité",
                price: 20
            },
            {
                name: "Fixation extincteur",
                price: 72
            },
            {
                name: "Support caméra sport",
                price: 89
            },
            {
                name: "Support smartphone magnétique-Fixation sur tableeau de bord",
                price: 21
            },
            {
                name: "Tapis de coffre",
                price: 83
            },
            {
                name: "Filet de rangement horizontal",
                price: 59
            },
            {
                name: "Chargeur batterie",
                price: 240
            },
            {
                name: "Kit d'outils alpine",
                price: 398
            },
            {
                name: "Cabochons Alpine métalisés",
                price: 24
            },
            {
                name: "Housse de protection Alpine",
                price: 216
            },
            {
                name: "Antivols pour jante noir",
                price: 51
            },
        ]



        this.RimsStandardSelected = this.RimsStandardSelected.bind(this);
        this.RimsSeracSelected = this.RimsSeracSelected.bind(this);
        this.RimsLegendeSelected = this.RimsLegendeSelected.bind(this);

        this.SeatDinamicaPureSelected = this.SeatDinamicaPureSelected.bind(this);
        this.SeatConfortPureSelected = this.SeatConfortPureSelected.bind(this);
        this.SeatConfortLegendeSelected = this.SeatConfortLegendeSelected.bind(this);
        this.SeatBrunLegendeSelected = this.SeatBrunLegendeSelected.bind(this);

        this.initCarousel = this.initCarousel.bind(this);
    }

// FONCTION DE SELECTION MODELE PURE
    onModelSelect(i) {
        this.setState(state => ({modele: this.modele[i].name, price: this.modele[i].price,color: "no color choose", rims: "No rims choose", seat: "No seat choose"}))
    }

    // FONCTION DE SELECTION COULEUR : BLEUE ALPINE
    onColorSelect(i){
        this.setState(state => ({color: this.color[i].name, colorPrice: this.color[i].price }))
    }

    // FONCTION DE SELECTION JANTES : STANDARD
    RimsStandardSelected(){
        this.setState(state => ({rims: this.rims[0].name, rimsPrice: this.rims[0].price }))
    }

    //FONCTION DE SELECTION JANTES : SERAC
    RimsSeracSelected(){
        this.setState(state => ({rims: this.rims[1].name, rimsPrice: this.rims[1].price }))
    }

    // FONCTION DE SELECTION JANTES : LEGENDE
    RimsLegendeSelected(){
        this.setState(state => ({rims: this.rims[2].name, rimsPrice: this.rims[2].price }))
    }

    //FONCTION DE SELECTION SIEGES : DINAMICA PURE
    SeatDinamicaPureSelected(){
        this.setState(state => ({seat: this.seat[0].name, seatPrice: this.seat[0].price }))
    }

    //FONCTION DE SELECTION SIEGES : CONFORT PURE
    SeatConfortPureSelected(){
        this.setState(state => ({seat: this.seat[1].name, seatPrice: this.seat[1].price }))
    }

    // FONCTION DE SELECTION SIEGES : CONFORT LEGENDE
    SeatConfortLegendeSelected(){
        this.setState(state => ({seat: this.seat[2].name, seatPrice: this.seat[2].price }))
    }

    //FONCTION DE SELECTION SIEGES : BRUN LEGENDE
    SeatBrunLegendeSelected(){
        this.setState(state => ({seat: this.seat[3].name, seatPrice: this.seat[3].price }))
    }

    // FONCTION DE SELECTION DES EQUIPEMENTS
    testOption(price,name, index) {
        let j;
        let compteurDeMerde = 0;
        let positionDeLaMerde;
        for(j = 0; j <= this.state.listEquipment.length; j++ ){
            if(this.state.listEquipment[j] === name){
                compteurDeMerde++;
                positionDeLaMerde = j;
            }
        }
        if(compteurDeMerde > 0){
            this.state.listEquipment.splice(positionDeLaMerde, 1);
            this.setState(state => ({equipmentPrice: this.state.equipmentPrice -= this.equipment[index].price}))
        }else{
            this.setState(state => ({listEquipment: this.state.listEquipment.concat(name)}));
            this.setState(state => ({equipmentPrice: this.state.equipmentPrice += price}))
            return;
        }
    }

    //FONCTION DE SELECTION DES ACCESSOIRES
    accessorieSelected(price, name, index) {
        let i;
        let compteurCaca = 0;
        let positionCaca;
        for(i = 0; i <= this.state.listAccessorie.length; i++){
            if(this.state.listAccessorie[i] === name){
                compteurCaca++;
                positionCaca = i;
            }
        }
        if(compteurCaca > 0){
            this.state.listAccessorie.splice(positionCaca, 1);
            this.setState(state => ({accessoriePrice: this.state.accessoriePrice -= this.accessorie[index].price}))
        } else {
            this.setState(state => ({listAccessorie: this.state.listAccessorie.concat(name)}));
            this.setState(state => ({accessoriePrice: this.state.accessoriePrice += price}))
            return;
        }
    }

    initCarousel(){
        const elems = document.querySelectorAll('.carousel');
        const instances = Carousel.init(elems);
    }

    render() {
        return (
            <div className="App">
                {/* NAVBAR */}
                <div className="navbar-fixed">
                    <nav className="navbar rgba-blue-strong fixed-top scrolling-navbar">
                        <a href="#" className="brand-logo"><img src="../assets/sources-homepage/logo/logo-white.png" width="180"/></a>
                        <a href="#" data-target="mobile-demo" className="sidenav-trigger"><i
                            className="material-icons">menu</i></a>
                        <ul className="right hide-on-med-and-down">
                            <li><a className='dropdown-trigger btn btn-nav' href='#' data-target='dropdown1'>Menu Home</a></li>
                        </ul>
                    </nav>
                </div>

                {/* HEAD */}
                <div className="head-configurateur taille-head">
                    {/*<img class="logo-configurateur" src="../assets/sources-homepage/logo/logo.png"/>*/}
                </div>

                <div className="head-text-configurateur">
                    <h5>Configurez votre Alpine selon vos préférences en sélectionnant parmi l'ensemble des couleurs,
                        équipements et accessoires proposés.</h5> <br/><br/>

                    <i>A110 Pure : l'esprit de la mythique berlinette / A110 Légende : le caractère et le confort d'une GT</i>
                </div>

                {/* MODELES */}
                <div className="row marg-top-configurateur">
                    <div className="col l4 m12 s12">
                        <ul className="collapsible">
                            <li className="width-button-collapsible">
                                <div className="collapsible-header center-h4-collapsible"><h4>Modèles</h4></div>
                                <div className="collapsible-body">
                                    <div className="row">
                                        <div className="col l6 m6 s12">
                                        <button
                                        type="button"
                                        title="A110 Pure"
                                        onClick={_ => {
                                            this.onModelSelect(0)
                                        }}
                                        data-position="top"
                                        data-tooltip={this.modele[0].name}
                                        className="select_modele_pure">
                                        <img className="padd-img-configurateur" src="../assets/configurateur/modele/selection/pure.png" alt="pure-img" width="50%" height="auto"/>
                                        </button>
                                        <p className="text-equipement-accessoires">Pure <br/>
                                            54 700€</p>
                                        </div>
                                        <div className="col l6 m6 s12">
                                        <button
                                            type="button"
                                            data-toggle="tooltip"
                                            title="A110 Legende"
                                            onClick={_ => {
                                                this.onModelSelect(1)
                                            }}
                                            data-position="top"
                                            data-tooltip={this.modele[1].name}
                                            className="select_modele_legende">
                                            <img className="padd-img-configurateur" src="../assets/configurateur/modele/selection/legende.png" width="50%" height="50%"/>
                                        </button>
                                        <p className="text-equipement-accessoires">Légende <br/>
                                            58 500€</p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        {/* COULEUR */}
                        <ul className="collapsible">
                            <li className="width-button-collapsible">
                                <div className="collapsible-header center-h4-collapsible"><h4>Couleur</h4></div>
                                <div className="collapsible-body">
                                    <div className="row">
                                        <div className="col l6 m6 s12">
                                            <button
                                                type="button"
                                                title="Peinture Bleue Alpine"
                                                onClick={_ => {
                                                    this.onColorSelect(0)
                                                }}
                                                data-position="top"
                                                data-tooltip={this.color[0].name}
                                                className="select_color_blue">
                                                <img className="padd-img-configurateur" src="../assets/configurateur/couleurs/selection/bleu.jpg" width="40%" height="50%"/>
                                            </button>
                                            <p className="text-equipement-accessoires">Bleu alpine <br/> 1800€</p>
                                        </div>
                                        <div className="col l6 m6 s12">
                                            <button
                                                type="button"
                                                title=" Peinture Noire Métalisée"
                                                onClick={_ => {
                                                    this.onColorSelect(1)
                                                }}
                                                data-position="top"
                                                data-tooltip={this.color[1].name}
                                                className="select_color_black">
                                                <img className="padd-img-configurateur" src="../assets/configurateur/couleurs/selection/noir.jpg" width="40%" height="50%"/>
                                            </button>
                                            <p className="text-equipement-accessoires">Noir métallisé <br/> 840€</p>
                                        </div>
                                    </div>
                                        <button
                                            type="button"
                                            title="Peinture Blanc Opaque"
                                            onClick={_ => {
                                                this.onColorSelect(2)
                                            }}
                                            data-position="top"
                                            data-tooltip={this.color[2].name}
                                            className="select_color_white">
                                            <img className="padd-img-configurateur" src="../assets/configurateur/couleurs/selection/blanc.jpg" width="40%" height="50%"/>
                                        </button>
                                        <p className="text-equipement-accessoires">Opaque blanc <br/> 0€</p>
                                </div>
                            </li>
                        </ul>

                        {/* JANTES */}
                        <ul className="collapsible">
                            <li className="width-button-collapsible">
                                <div className="collapsible-header center-h4-collapsible"><h4>Jantes</h4></div>
                                <div className="collapsible-body">
                                    {this.state.modele === this.modele[1].name && <button
                                        type="button"
                                        title="Jantes Legende"
                                        onClick={this.RimsLegendeSelected}
                                        data-position="top"
                                        data-tooltip={this.rims[2].name}
                                        className="select_rims_legend">
                                        <img className="padd-img-configurateur" src="../assets/configurateur/jantes/selection/jante-legende.jpg" width="40%" height="50%"/>
                                    </button>
                                    }

                                    {this.state.modele === this.modele[0].name &&
                                    <div className="row">
                                        <div className="col l6 m6 s12">
                                            <button
                                                type="button"
                                                title="Jantes Standard"
                                                onClick={this.RimsStandardSelected}
                                                data-position="top"
                                                data-tooltip={this.rims[0].name}
                                                className="select_rims_standard">
                                                <img className="padd-img-configurateur" src="../assets/configurateur/jantes/selection/jante-standard.jpg" width="40%" height="50%"/>
                                            </button>
                                            <p className="text-equipement-accessoires">Standard <br/> 0€</p>
                                        </div>

                                        <div className="col l6 m6 s12">
                                            <button
                                                type="button"
                                                title="Jantes Serac"
                                                onClick={this.RimsSeracSelected}
                                                data-position="top"
                                                data-tooltip={this.rims[1].name}
                                                className="select_rims_serac">
                                                <img className="padd-img-configurateur" src="../assets/configurateur/jantes/selection/jante-serac.jpg" width="40%" height="50%"/>
                                            </button>
                                            <p className="text-equipement-accessoires">Serac <br/> 1000€</p>
                                        </div>
                                    </div>
                                    }
                                </div>
                            </li>
                        </ul>

                        {/* SIEGES */}
                        <ul className="collapsible">
                            <li className="width-button-collapsible">
                                <div className="collapsible-header center-h4-collapsible"><h4>Sièges</h4></div>
                                <div className="collapsible-body">
                                    {this.state.modele === this.modele[0].name &&
                                    <div className="row">
                                        <div className="col l6 m6 s12">
                                            <button
                                                type="button"
                                                title="Sièges Dinamica"
                                                onClick={this.SeatDinamicaPureSelected}
                                                data-position="top"
                                                data-tooltip={this.seat[0].name}
                                                className="select_seat_dinamica">
                                                <img className="padd-img-configurateur" src="../assets/configurateur/interieurs/selection/cuir-noir_dinamica.jpg" width="40%" height="50%"/>
                                            </button>
                                            <p className="text-equipement-accessoires">Dinamica <br/> 0€</p>
                                        </div>
                                        <div className="col l6 m6 s12">
                                            <button
                                                type="button"
                                                title="Sièges Confort Pure"
                                                onClick={this.SeatConfortPureSelected}
                                                data-position="top"
                                                data-tooltip={this.seat[1].name}
                                                className="select_seat_perfore">
                                                <img className="padd-img-configurateur" src="../assets/configurateur/interieurs/selection/cuir-noir_perfore.jpg" width="40%" height="50%"/>
                                            </button>
                                            <p className="text-equipement-accessoires">Cuir noir <br/> 800€</p>
                                        </div>
                                    </div>
                                    }

                                    {this.state.modele === this.modele[1].name &&
                                    <div className="row">
                                        <div className="col l6 m6 s12">
                                            <button
                                                type="button"
                                                title="Sièges Confort pour version 'Legende'"
                                                onClick={this.SeatConfortLegendeSelected}
                                                data-position="top"
                                                data-tooltip={this.seat[2].name}
                                                className="select_seat_confort">
                                                <img className="padd-img-configurateur" src="../assets/configurateur/interieurs/selection/cuir-noir.jpg" width="40%" height="50%"/>
                                            </button>
                                            <p className="text-equipement-accessoires">Cuir noir <br/> 0€</p>
                                        </div>

                                        <div className="col l6 m6 s12">
                                            <button
                                                type="button"
                                                title="Sièges Bruns pour version 'Legende'"
                                                onClick={this.SeatBrunLegendeSelected}
                                                data-position="top"
                                                data-tooltip={this.seat[3].name}
                                                className="select_seat_brun">
                                                <img className="padd-img-configurateur" src="../assets/configurateur/interieurs/selection/cuir-brun.jpg" width="40%" height="50%"/>
                                            </button>
                                            <p className="text-equipement-accessoires">Cuir brun <br/> 800€</p>
                                        </div>
                                    </div>
                                    }
                                </div>
                            </li>
                        </ul>

                        {/* EQUIPEMENTS */}
                        <div className="row">
                            <div className="col l6 s12 m6">
                                <Modal
                                    bottomSheet
                                    trigger={<Button className={"btn-configurateur"}>Equipements</Button>}>
                                    <div className="Equipements">
                                        <h4 class="title-configurateur">Equipements</h4>
                                        <div className="row">
                                            <ul>
                                                <div className="col l3">
                                                    <h6 className="title-equipement">Personnalisation intérieur</h6>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[20].name + this.equipment[20].price + "€"}>
                                                        <input
                                                            type="checkbox" id="cb21"
                                                            onChange={() => {
                                                                this.testOption(this.equipment[20].price, this.equipment[20].name, 20)
                                                            }}/>
                                                        <label htmlFor="cb21"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/personnalisation_interieure/pedal-alu.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Pédalier alu <br/> 120€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[21].name + this.equipment[21].price + "€"}>
                                                        <input
                                                            type="checkbox" id="cb22"
                                                            onChange={() => {
                                                                this.testOption(this.equipment[21].price, this.equipment[21].name, 21)
                                                            }}/>
                                                        <label htmlFor="cb22"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/personnalisation_interieure/pack-carbone.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Pack carbone <br/> 0€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[22].name + this.equipment[22].price + "€"}>
                                                        <input
                                                            type="checkbox" id="cb23"
                                                            onChange={() => {
                                                                this.testOption(this.equipment[22].price, this.equipment[22].name, 22)
                                                            }}/>
                                                        <label htmlFor="cb23"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/personnalisation_interieure/logo-volant.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Logo volant <br/> 84€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[23].name + this.equipment[23].price + "€"}>
                                                        <input
                                                            type="checkbox" id="cb24"
                                                            onChange={() => {
                                                                this.testOption(this.equipment[23].price, this.equipment[23].name, 23)
                                                            }}/>
                                                        <label htmlFor="cb24"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/personnalisation_interieure/siege-chauffant.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Sièges chauffants <br/> 400€</p>
                                                    </li>
                                                </div>

                                                <div className="col l3">
                                                    <h6 class="title-equipement">Média & Navigation</h6>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[2].name + this.equipment[2].price + "€"}><input
                                                        type="checkbox" id="cb3"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[2].price, this.equipment[2].name, 2)
                                                        }}/>
                                                        <label htmlFor="cb3"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/media_et_navigation/alpine-metrics.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Alpine metrics <br/> 204€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[3].name + this.equipment[3].price + "€"}><input
                                                        type="checkbox" id="cb4"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[3].price, this.equipment[3].name, 3)
                                                        }}/>
                                                        <label htmlFor="cb4"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/media_et_navigation/audio-focal.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Audio focal <br/> 600€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[4].name + this.equipment[4].price + "€"}><input
                                                        type="checkbox" id="cb5"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[4].price, this.equipment[4].name, 4)
                                                        }}/>
                                                        <label htmlFor="cb5"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/media_et_navigation/audio-premium.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Audio prenium <br/> 1200€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[5].name + this.equipment[5].price + "€"}><input
                                                        type="checkbox" id="cb6"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[5].price, this.equipment[5].name, 5)
                                                        }}/>
                                                        <label htmlFor="cb6"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/media_et_navigation/audio-standard.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Audio standard <br/> 0€</p>
                                                    </li>
                                                </div>

                                                <div className="col l3">
                                                    <h6 class="title-equipement">Confort</h6>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[6].name + this.equipment[6].price + "€"}><input
                                                        type="checkbox" id="cb7"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[6].price, this.equipment[6].name, 6)
                                                        }}/>
                                                        <label htmlFor="cb7"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/confort/retro-int-electrochrome.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Electrochrome <br /> 0€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[7].name + this.equipment[7].price + "€"}><input
                                                        type="checkbox" id="cb8"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[7].price, this.equipment[7].name, 7)
                                                        }}/>
                                                        <label htmlFor="cb8"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/confort/retro-ext-chaffant.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Chaffant <br /> 504€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[9].name + this.equipment[9].price + "€"}><input
                                                        type="checkbox" id="cb10"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[9].price, this.equipment[9].name, 9)
                                                        }}/>
                                                        <label htmlFor="cb10"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/confort/regul-limit-vitesse.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Régulateur de <br/> vitesse <br /> 0€</p>
                                                    </li>
{/*                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[8].name + this.equipment[8].price + "€"}><input
                                                        type="checkbox" id="cb9"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[8].price, this.equipment[8].name, 8)
                                                        }}/>
                                                        <label htmlFor="cb9"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/confort/pack-rangement.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Rangement <br/>
                                                            504€</p>
                                                    </li> */}
                                                </div>

                                                <div className="col l3">
                                                    <h6 class="title-equipement">Conduite</h6>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[10].name + this.equipment[10].price + "€"}><input
                                                        type="checkbox" id="cb11"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[10].price, this.equipment[10].name, 10)
                                                        }}/>
                                                        <label htmlFor="cb11"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/conduite/aide-stationnement-ar.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Stationnement AR <br/> 420€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[11].name + this.equipment[11].price + "€"}><input
                                                        type="checkbox" id="cb12"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[11].price, this.equipment[11].name, 11)
                                                        }}/>
                                                        <label htmlFor="cb12"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/conduite/aide-stationnement-av-ar.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Av et AR <br/> 750€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[12].name + this.equipment[12].price + "€"}><input
                                                        type="checkbox" id="cb13"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[12].price, this.equipment[12].name, 12)
                                                        }}/>
                                                        <label htmlFor="cb13"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/conduite/camera-recul.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Caméra recul <br/> 1200€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[13].name + this.equipment[13].price + "€"}><input
                                                        type="checkbox" id="cb14"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[13].price, this.equipment[13].name, 13)
                                                        }}/>
                                                        <label htmlFor="cb14"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/conduite/echappement-sport.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Echappement sport <br/> 1500€</p>
                                                    </li>
                                                </div>
                                            </ul>
                                        </div>

                                        <div className="row">
                                            <ul>
                                                <div className="col l3">
                                                    <h6 class="title-equipement">Sécurité</h6>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[14].name + this.equipment[14].price + "€"}><input
                                                        type="checkbox" id="cb15"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[14].price, this.equipment[14].name, 14)
                                                        }}/>
                                                        <label htmlFor="cb15"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/securite/freinage-haute-perf.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Freinage <br/> 1000€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[15].name + this.equipment[15].price + "€"}><input
                                                        type="checkbox" id="cb16"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[15].price, this.equipment[15].name, 15)
                                                        }}/>
                                                        <label htmlFor="cb16"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/securite/aide-freinage-durgence.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Urgence <br /> 0€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[16].name + this.equipment[16].price + "€"}><input
                                                        type="checkbox" id="cb17"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[16].price, this.equipment[16].name, 16)
                                                        }}/>
                                                        <label htmlFor="cb17"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/securite/airbag.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Airbag <br/> 0€</p>
                                                    </li>
                                                </div>

                                                <div className="col l3">
                                                    <h6 class="title-equipement">Personnalisation extérieur</h6>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[17].name + this.equipment[17].price + "€"}><input
                                                        type="checkbox" id="cb18"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[17].price, this.equipment[17].name, 17)
                                                        }}/>
                                                        <label htmlFor="cb18"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/personnalisation_exterieure/etrier-bleu.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Etrier bleu <br/> 384€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[18].name + this.equipment[18].price + "€"}><input
                                                        type="checkbox" id="cb19"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[18].price, this.equipment[18].name, 18)
                                                        }}/>
                                                        <label htmlFor="cb19"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/personnalisation_exterieure/logo-alpine.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Logo alpine <br/> 120€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[19].name + this.equipment[19].price + "€"}><input
                                                        type="checkbox" id="cb20"
                                                        onChange={() => {
                                                            this.testOption(this.equipment[19].price, this.equipment[19].name, 19)
                                                        }}/>
                                                        <label htmlFor="cb20"><img class="border-img-equpement" src="../assets/configurateur/equipements/categories/personnalisation_exterieure/etrier-gris.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Etrier gris <br/> 0€</p>
                                                    </li>
                                                </div>

                                                <div className="col l3">
                                                    <h6 className="title-equipement">Design</h6>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[0].name + this.equipment[0].price + "€"}>
                                                        <input
                                                            type="checkbox" id="cb1"
                                                            onChange={() => {
                                                                this.testOption(this.equipment[0].price, this.equipment[0].name, 0)
                                                            }}/>
                                                        <label htmlFor="cb1"><img
                                                            class="border-img-equpement" src="../assets/configurateur/equipements/categories/design/pack-heritage.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Pack héritage <br/> 180€</p>
                                                    </li>
                                                    <li
                                                        className="tooltipped"
                                                        data-position="top"
                                                        data-tooltip={this.equipment[1].name + this.equipment[1].price + "€"}>
                                                        <input
                                                            type="checkbox" id="cb2"
                                                            onChange={() => {
                                                                this.testOption(this.equipment[1].price, this.equipment[1].name, 1)
                                                            }}/>
                                                        <label htmlFor="cb2"><img
                                                            class="border-img-equpement" src="../assets/configurateur/equipements/categories/design/repose-pied-alu.jpg"/></label>
                                                        <p className="text-equipement-accessoires">Repose pieds alu<br/> 96€</p>
                                                    </li>
                                                </div>
                                            </ul>
                                        </div>
                                    </div>
                                </Modal>
                            </div>

                            <div className="col l6 s12 m6 ">
                            {/* ACCESSOIRES */}
                                <Modal
                                    bottomSheet
                                    trigger={<Button className={"btn-configurateur"}>Accessoires</Button>}>
                                        <div className="Accessories">
                                            <h4 className="title-configurateur">Accessoires</h4>
                                            <div className="row">
                                                <ul>
                                                    <div className="col l3">
                                                        <h6 class="title-equipement"> Multimédia </h6>
                                                        <li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[6].name + this.accessorie[6].price + "€"}><input type="checkbox" id="bc7" onChange={() => {this.accessorieSelected(this.accessorie[6].price, this.accessorie[6].name, 6)}}/>
                                                            <label for="bc7"><img class="border-img-equpement" src="../assets/configurateur/accessoires/multimedia/support-camera.jpg"/></label>
                                                            <p className="text-equipement-accessoires">Support caméra <br/> 89€</p>
                                                        </li>
                                                        <li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[7].name + this.accessorie[7].price + "€"}><input type="checkbox" id="bc8" onChange={() => {this.accessorieSelected(this.accessorie[7].price, this.accessorie[7].name, 7)}}/>
                                                            <label for="bc8"><img class="border-img-equpement" src="../assets/configurateur/accessoires/multimedia/support-smartphone.jpg"/></label>
                                                            <p className="text-equipement-accessoires">Support téléphone <br/> 21€</p>
                                                        </li>
                                                    </div>

                                                    <div className="col l3">
                                                        <h6 class="title-equipement"> Intérieur </h6>
                                                        <li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[8].name + this.accessorie[8].price + "€"}><input type="checkbox" id="bc9" onChange={() => {this.accessorieSelected(this.accessorie[8].price, this.accessorie[8].name, 8)}}/>
                                                            <label for="bc9"><img class="border-img-equpement" src="../assets/configurateur/accessoires/interieur/tapis-coffre.jpg"/></label>
                                                            <p className="text-equipement-accessoires">Tapis de coffre <br/> 83€</p>
                                                        </li>
                                                        <li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[9].name + this.accessorie[9].price + "€"}><input type="checkbox" id="bc10" onChange={() => {this.accessorieSelected(this.accessorie[9].price, this.accessorie[9].name, 9)}}/>
                                                            <label for="bc10"><img class="border-img-equpement" src="../assets/configurateur/accessoires/interieur/filet-rangement.jpg"/></label>
                                                            <p className="text-equipement-accessoires">Filet de rangement <br/> 59€</p>
                                                        </li>
                                                    </div>
                                                    <div className="col l3">
                                                        <h6 class="title-equipement"> Matériel de garage </h6>
                                                        <li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[10].name + this.accessorie[10].price + "€"}><input type="checkbox" id="bc11" onChange={() => {this.accessorieSelected(this.accessorie[10].price, this.accessorie[10].name, 10)}}/>
                                                            <label for="bc11"><img class="border-img-equpement" src="../assets/configurateur/accessoires/garage/chargeur-batterie.jpg"/></label>
                                                            <p className="text-equipement-accessoires">Chargeur batterie <br/> 240€</p>
                                                        </li>
                                                        <li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[11].name + this.accessorie[11].price + "€"}><input type="checkbox" id="bc12" onChange={() => {this.accessorieSelected(this.accessorie[11].price, this.accessorie[11].name, 11)}}/>
                                                            <label for="bc12"><img class="border-img-equpement" src="../assets/configurateur/accessoires/garage/kit-outils.jpg"/></label>
                                                            <p className="text-equipement-accessoires">Kit outils <br/> 398€</p>
                                                        </li>
                                                    </div>

                                                    <div className="col l3">
                                                        <h6 className="title-equipement"> Extérieur </h6>
                                                        <li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[12].name + this.accessorie[12].price + "€"}>
                                                            <input type="checkbox" id="bc13" onChange={() => {
                                                                this.accessorieSelected(this.accessorie[12].price, this.accessorie[12].name, 12)
                                                            }}/>
                                                            <label htmlFor="bc13"><img class="border-img-equpement" src="../assets/configurateur/accessoires/exterieur/cabochons-metal.jpg"/></label>
                                                            <p className="text-equipement-accessoires">Cabochons <br/> 24€</p>
                                                        </li>
                                                        <li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[13].name + this.accessorie[13].price + "€"}>
                                                            <input type="checkbox" id="bc14" onChange={() => {
                                                                this.accessorieSelected(this.accessorie[13].price, this.accessorie[13].name, 13)
                                                            }}/>
                                                            <label htmlFor="bc14"><img
                                                                class="border-img-equpement" src="../assets/configurateur/accessoires/exterieur/housse.jpg"/></label>
                                                            <p className="text-equipement-accessoires">Housse <br/> 216€</p>
                                                        </li>
                                                        {/*<li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[14].name + this.accessorie[14].price + "€"}>
                                                            <input type="checkbox" id="bc15" onChange={() => {
                                                                this.accessorieSelected(this.accessorie[14].price, this.accessorie[14].name, 14)
                                                            }}/>
                                                            <label htmlFor="bc15"><img
                                                                class="border-img-equpement" src="../assets/configurateur/accessoires/exterieur/antivol-jantes.jpg"/></label>
                                                        </li>*/}
                                                    </div>
                                                </ul>
                                            </div>
                                            <div className="row">
                                                <ul>
                                                    <div className="col l12">
                                                        <h6 class="title-equipement"> Transport & Protection </h6>
                                                        <li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[0].name + this.accessorie[0].price + "€"}>
                                                            <input type="checkbox" id="bc1" onChange={() => {
                                                                this.accessorieSelected(this.accessorie[0].price, this.accessorie[0].name, 0)
                                                            }}/>
                                                            <label htmlFor="bc1"><img class="border-img-equpement" src="../assets/configurateur/accessoires/transport_et_protection/extincteur.jpg"/></label>
                                                            <p class="text-equipement-accessoires">Extincteur <br /> 22€</p>
                                                        </li>
                                                        <li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[1].name + this.accessorie[1].price + "€"}>
                                                            <input type="checkbox" id="bc2" onChange={() => {
                                                                this.accessorieSelected(this.accessorie[1].price, this.accessorie[1].name, 1)
                                                            }}/>
                                                            <label htmlFor="bc2"><img class="border-img-equpement" src="../assets/configurateur/accessoires/transport_et_protection/chaaine-neige.jpg"/></label>
                                                            <p className="text-equipement-accessoires">Chaine neige <br/> 336€</p>
                                                        </li>
                                                        <li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[2].name + this.accessorie[2].price + "€"}>
                                                            <input type="checkbox" id="bc3" onChange={() => {
                                                                this.accessorieSelected(this.accessorie[2].price, this.accessorie[2].name, 2)
                                                            }}/>
                                                            <label htmlFor="bc3"><img class="border-img-equpement" src="../assets/configurateur/accessoires/transport_et_protection/alarme.jpg"/></label>
                                                            <p className="text-equipement-accessoires">Alarme <br/> 543€</p>
                                                        </li>
                                                        <li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[3].name + this.accessorie[3].price + "€"}>
                                                            <input type="checkbox" id="bc4" onChange={() => {
                                                                this.accessorieSelected(this.accessorie[3].price, this.accessorie[3].name, 3)
                                                            }}/>
                                                            <label htmlFor="bc4"><img class="border-img-equpement" src="../assets/configurateur/accessoires/transport_et_protection/protection-obd.jpg"/></label>
                                                            <p className="text-equipement-accessoires">Protection prise <br/> 99€</p>
                                                        </li>
                                                        <li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[4].name + this.accessorie[4].price + "€"}>
                                                            <input type="checkbox" id="bc5" onChange={() => {
                                                                this.accessorieSelected(this.accessorie[4].price, this.accessorie[4].name, 4)
                                                            }}/>
                                                            <label htmlFor="bc5"><img class="border-img-equpement" src="../assets/configurateur/accessoires/transport_et_protection/kit-securite.jpg"/></label>
                                                            <p className="text-equipement-accessoires">Kit de sécurité  <br/> 20€</p>
                                                        </li>
                                                        <li
                                                            className="tooltipped"
                                                            data-position="top"
                                                            data-tooltip={this.accessorie[5].name + this.accessorie[5].price + "€"}>
                                                            <input type="checkbox" id="bc6" onChange={() => {
                                                                this.accessorieSelected(this.accessorie[5].price, this.accessorie[5].name, 5)
                                                            }}/>
                                                            <label htmlFor="bc6"><img class="border-img-equpement" src="../assets/configurateur/accessoires/transport_et_protection/fixation-extincteur.jpg"/></label>
                                                            <p className="text-equipement-accessoires">Fixation extincteur <br/> 72€</p>
                                                        </li>
                                                    </div>
                                                </ul>
                                            </div>
                                        </div>
                                </Modal>
                            </div>
                        </div>
                    </div>

                    <h5 className="title-configurateur"> Choisissez votre modèle </h5>

                    <div className="col l8" id="task_flyout">
                        <div className="Visuel_voiture fixe-carousel">

                            {/* CONDIITION DE CHOIX DE MODELES */}
                            {this.state.modele === this.modele[0].name && (this.state.color === 'no color choose' || this.state.color === this.color[0].name) && this.state.rims === 'No rims choose' &&
                            <Carousel  options={{ fullWidth: true }} showArrows={true} images={[
                                '../assets/configurateur/modele/pure/modele_pure-couleur_bleu-jante_standard (1).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_bleu-jante_standard (2).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_bleu-jante_standard (3).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_bleu-jante_standard (4).jpg',
                            ]} />
                            }

                            {this.state.modele === this.modele[0].name && this.state.color === this.color[1].name && this.state.rims === 'No rims choose' &&
                            <Carousel options={{ fullWidth: true }} showArrows={true} images={[
                                '../assets/configurateur/modele/pure/modele_pure-couleur_noire-jante_standard (1).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_noire-jante_standard (2).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_noire-jante_standard (3).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_noire-jante_standard (4).jpg',
                            ]} />
                            }

                            {this.state.modele === this.modele[0].name && this.state.color === this.color[2].name && this.state.rims === 'No rims choose' &&
                            <Carousel options={{ fullWidth: true }} showArrows={true} images={[
                                '../assets/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (1).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (2).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (3).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (4).jpg',
                            ]} />
                            }

                            {this.state.modele === this.modele[1].name && (this.state.color === 'no color choose' || this.state.color === this.color[0].name) && this.state.rims === 'No rims choose' &&
                            <Carousel options={{ fullWidth: true }} showArrows={true} images={[
                                '../assets/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-4.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-2.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-3.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-1.jpg',
                            ]} />
                            }

                            {this.state.modele === this.modele[1].name && this.state.color === this.color[1].name && this.state.rims === 'No rims choose' &&
                            <Carousel options={{ fullWidth: true }} showArrows={true} images={[
                                '../assets/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-4.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-2.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-3.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-1.jpg',
                            ]} />
                            }

                            {this.state.modele === this.modele[1].name && this.state.color === this.color[2].name && this.state.rims === 'No rims choose' &&
                            <Carousel options={{ fullWidth: true }} showArrows={true} images={[
                                '../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-4.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-2.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-3.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-1.jpg',
                            ]} />

                            }

                            {/* CONDITIONS POUR LE MODELE "PURE" :   */}
                            {this.state.modele === this.modele[0].name && this.state.color === this.color[2].name && this.state.rims === this.rims[1].name &&

                            <Carousel options={{ fullWidth: true }} showArrows={true} images={[
                                '../assets/configurateur/modele/pure/modele_pure-couleur_blanche-jante_serac_(3).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_blanche-jante_serac_(2).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_blanche-jante_serac_(1).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_blanche-jante_serac_(4).jpg',

                            ]} />
                            }
                            {this.state.modele === this.modele[0].name && this.state.color === this.color[2].name && this.state.rims === this.rims[0].name &&

                            <Carousel options={{ fullWidth: true }} showArrows={true} images={[
                                '../assets/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (3).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (2).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (1).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_blanche-jante_standard (4).jpg',

                            ]} />
                            }
                            {this.state.modele === this.modele[0].name && this.state.color === this.color[0].name && this.state.rims === this.rims[0].name &&

                            <Carousel options={{ fullWidth: true }} showArrows={true} images={[
                                '../assets/configurateur/modele/pure/modele_pure-couleur_bleu-jante_standard (3).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_bleu-jante_standard (2).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_bleu-jante_standard (1).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_bleu-jante_standard (4).jpg',

                            ]} />
                            }
                            {this.state.modele === this.modele[0].name && this.state.color === this.color[0].name && this.state.rims === this.rims[1].name &&

                            <Carousel options={{ fullWidth: true }} showArrows={true} images={[
                                '../assets/configurateur/modele/pure/modele_pure-couleur_bleu-jante_serac (3).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_bleu-jante_serac (2).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_bleu-jante_serac (1).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_bleu-jante_serac (4).jpg',

                            ]} />
                            }
                            {this.state.modele === this.modele[0].name && this.state.color === this.color[1].name && this.state.rims === this.rims[0].name &&

                            <Carousel options={{ fullWidth: true }} showArrows={true} images={[
                                '../assets/configurateur/modele/pure/modele_pure-couleur_noire-jante_standard (3).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_noire-jante_standard (2).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_noire-jante_standard (1).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_noire-jante_standard (4).jpg',

                            ]} />
                            }
                            {this.state.modele === this.modele[0].name && this.state.color === this.color[1].name && this.state.rims === this.rims[1].name &&

                            <Carousel options={{ fullWidth: true }} showArrows={true} images={[

                                '../assets/configurateur/modele/pure/modele_pure-couleur_noire-jante_serac (3).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_noire-jante_serac (2).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_noire-jante_serac (1).jpg',
                                '../assets/configurateur/modele/pure/modele_pure-couleur_noire-jante_serac (4).jpg',

                            ]} />
                            }

                            {/* CONDITIONS POUR LE MODELE "LEGENDE" */}

                            {this.state.modele === this.modele[1].name && this.state.color === this.color[0].name && this.state.rims === this.rims[2].name &&

                            <Carousel options={{ fullWidth: true }} showArrows={true} images={[
                                '../assets/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-4.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-2.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-3.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_bleu-jante_legende-1.jpg',
                            ]} />
                            }
                            {this.state.modele === this.modele[1].name && this.state.color === this.color[1].name && this.state.rims === this.rims[2].name &&

                            <Carousel options={{ fullWidth: true }} showArrows={true} images={[
                                '../assets/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-4.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-2.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-3.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_noir-jante_legende-1.jpg',
                            ]} />
                            }
                            {this.state.modele === this.modele[1].name && this.state.color === this.color[2].name && this.state.rims === this.rims[2].name &&

                            <Carousel options={{ fullWidth: true }} showArrows={true} images={[
                                '../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-4.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-2.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-3.jpg',
                                '../assets/configurateur/modele/legende/modele_legende-couleur_blanc-jante_legende-1.jpg',
                            ]} />
                            }
                        </div>
                    </div>
                </div>

                    <div className="row">
                        <div className="col l4 m12 s12">
                            <Modal
                                header='Récapitulatif de votre commande'
                                trigger={<Button className={"btn-configurateur"}>Récapitulatif</Button>}>
                                Configurateur :
                                 {(this.state.price + this.state.colorPrice + this.state.rimsPrice + this.state.seatPrice + this.state.equipmentPrice + this.state.accessoriePrice)}€
                                <br></br>
                                Modèles: {this.state.modele}
                                <br></br>
                                Couleur: {this.state.color}
                                <br></br>
                                Jantes: {this.state.rims}
                                <br></br>
                                Sièges: {this.state.seat}
                                <br></br>
                                Equipements : {this.state.listEquipment + ' '}
                                <br></br>
                                Accessoires : {this.state.listAccessorie + ' '}
                                <br></br>
                            </Modal>
                        </div>
                    </div>
                <footer>
                    <div className="footer">
                        <div className="footer_center">
                            <h5 className="text-footer">Copyright © MoYvalpine by Morgan V et Yverick DC</h5>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('app'));

