document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems, options);
});

// Or with jQuery
$('.dropdown-trigger').dropdown();

// Or with jQuery

$(document).ready(function(){
    $('.sidenav').sidenav();
});

$(window).scroll(function () {
    var sc = $(window).scrollTop()
    if (sc > 630) {
        $(".rgba-blue-strong").addClass("navbar-scroll")
    }
    else {
        $(".rgba-blue-strong").removeClass("navbar-scroll")
    }
});

// PreHomePage
$(window).ready(function(){
    $(".boton").wrapInner('<div class=botontext></div>');

    $(".botontext").clone().appendTo( $(".boton") );

    $(".boton").append('<span class="twist"></span><span class="twist"></span><span class="twist"></span><span class="twist"></span>');

    $(".twist").css("width", "25%").css("width", "+=3px");
});

$(document).ready(function() {
    $("#button").click(function() {
        $("#triangle-down, #triangle-up").addClass("usenand");
        $('content').addClass('krassInefade');
        $("#triangle-up, #triangle-down").delay(500).fadeOut();
        $("#title").fadeOut(500);
        $("#test").fadeOut(500);
        $("#button").addClass("toMenu");
        $("#button").text('+');
        setTimeout(function() {
            $("#button").attr("id", "menu");
        }, 1);
    });

    $("#menuClose, #menuContent a").click(function() {
        $("#menu").removeClass("menuTransition");
        $("#menuContent").fadeOut(300);
        $("#menu").text('+');
        $("#menu.toMenu").css('transition', '0.3s ease-in-out');
    });

    $('a').click(function() {
        $('html, body').delay(500).animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 1000);
        return false;
    });

});

$(document).on("click", "#menu", function() {
    $("#menu").addClass("menuTransition");
    $("#menuContent").delay(500).fadeIn(1);
    $("#menu").text(' ');
});
// PreHomePage


function autoType(elementClass, typingSpeed){
    var thhis = $(elementClass);
    thhis.css({
        "position": "relative",
        "display": "inline-block"
    });
    thhis.prepend('<div class="cursor" style="right: initial; left:0;"></div>');
    thhis = thhis.find(".text-js");
    var text = thhis.text().trim().split('');
    var amntOfChars = text.length;
    var newString = "";
    thhis.text("|");
    setTimeout(function(){
        thhis.css("opacity",1);
        thhis.prev().removeAttr("style");
        thhis.text("");
        for(var i = 0; i < amntOfChars; i++){
            (function(i,char){
                setTimeout(function() {
                    newString += char;
                    thhis.text(newString);
                },i*typingSpeed);
            })(i+1,text[i]);
        }
    },1500);
}

$(document).ready(function(){
    // Now to start autoTyping just call the autoType function with the
    // class of outer div
    // The second paramter is the speed between each letter is typed.
    autoType(".type-js",200);
});

// Slider Section 3
document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.slider');
    var instances = M.Slider.init(elems, options);
});

$(document).ready(function(){
    $('.slider').slider();
});



/*
consoleText(['Copyright ©  MoYvalpine by Morgan V et Yverick DC'], 'text',['#fff']);

function consoleText(words, id, colors) {
    if (colors === undefined) colors = ['#fff'];
    var visible = true;
    var con = document.getElementById('console');
    var letterCount = 1;
    var x = 1;
    var waiting = false;
    var target = document.getElementById(id)
    target.setAttribute('style', 'color:' + colors[0])
    window.setInterval(function() {

        if (letterCount === 0 && waiting === false) {
            waiting = true;
            target.innerHTML = words[0].substring(0, letterCount)
            window.setTimeout(function() {
                var usedColor = colors.shift();
                colors.push(usedColor);
                var usedWord = words.shift();
                words.push(usedWord);
                x = 1;
                target.setAttribute('style', 'color:' + colors[0])
                letterCount += x;
                waiting = false;
            }, 1000)
        } else if (letterCount === words[0].length + 1 && waiting === false) {
            waiting = true;
            window.setTimeout(function() {
                x = -1;
                letterCount += x;
                waiting = false;
            }, 1000)
        } else if (waiting === false) {
            target.innerHTML = words[0].substring(0, letterCount)
            letterCount += x;
        }
    }, 120)
    window.setInterval(function() {
        if (visible === true) {
            con.className = 'console-underscore hidden'
            visible = false;

        } else {
            con.className = 'console-underscore'

            visible = true;
        }
    }, 400)
}
*/



/*Header Home*/
$(document).ready(function() {
    $(window).on('scroll', function() {
        if($(window).scrollTop() < 1000) {
            $('.hero').css('background-size', 130 + parseInt($(window).scrollTop() / 5) + '%');
            $('.hero h1').css('top', 25 + ($(window).scrollTop() * .1) + '%');
            $('.hero h1').css('opacity', 1 - ($(window).scrollTop() * .003));
        }

        if($(window).scrollTop() >= $('.content-wrapper').offset().top - 300) {
            $('.nav-bg').removeClass('bg-hidden');
            $('.nav-bg').addClass('bg-visible');
        } else {
            $('.nav-bg').removeClass('bg-visible');
            $('.nav-bg').addClass('bg-hidden');
        }
    });
});

var divider = document.getElementById("divider"),
    slider = document.getElementById("slider");
window.moveDivider = function() {
    divider.style.width = slider.value+"%";
}

/* Effet img techonologie */
$(".hover").mouseleave(
    function () {
        $(this).removeClass("hover");
    }
);

$(document).ready(function(){
    $('.parallax').parallax();
});


$(function() {
    $(".img-w").each(function() {
        $(this).wrap("<div class='img-c'></div>")
        let imgSrc = $(this).find("img").attr("src");
        $(this).css('background-image', 'url(' + imgSrc + ')');
    })


    $(".img-c").click(function() {
        let w = $(this).outerWidth()
        let h = $(this).outerHeight()
        let x = $(this).offset().left
        let y = $(this).offset().top


        $(".active").not($(this)).remove()
        let copy = $(this).clone();
        copy.insertAfter($(this)).height(h).width(w).delay(500).addClass("active")

        $(".active").css('left', x - 8);
        setTimeout(function() {
            copy.addClass("positioned")
        }, 0)

    })
})

$(document).on("click", ".img-c.active", function() {
    let copy = $(this)
    copy.removeClass("positioned active").addClass("postactive")
    setTimeout(function() {
        copy.remove();
    }, 500)
})


/*EFFET TEXTE AUTO ECRITURE*/

/*function typeEffect(element, speed) {
    var text = $(element).text();
    $(element).html('');

    var i = 0;
    var timer = setInterval(function() {
        if (i < text.length) {
            $(element).append(text.charAt(i));
            i++;
        } else {
            clearInterval(timer);
        }
    }, speed);
}

$( document ).ready(function() {
    var speed = 75;
    var delay = $('.versions-title-block').text().length * speed + speed;
    typeEffect($('.versions-title-block'), speed);
    setTimeout(function(){
        $('.text-versions').css('display', 'inline-block');
        typeEffect($('.text-versions'), speed);
    }, delay);
});

$( document ).ready(function() {
    var speed = 75;
    var delay = $('.versions-title-block2').text().length * speed + speed;
    typeEffect($('.versions-title-block2'), speed);
    setTimeout(function(){
        $('.text-versions2').css('display', 'inline-block');
        typeEffect($('.text-versions2'), speed);
    }, delay);
});*/

/*CHOIX BLOCK CONFIGURATEUR*/
$(document).ready(function(){
    $('.collapsible').collapsible();
});

/*Fixe carousel au scroll*/
$(window).scroll(function(){
    if ($(this).scrollTop() > 550) {
        $('#task_flyout').addClass('fixed');
    } else {
        $('#task_flyout').removeClass('fixed');
    }
});